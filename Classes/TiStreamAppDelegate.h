//
//  TiStreamAppDelegate.h
//  TiStream
//
//  Created by Patrick Roy on 11-02-24.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TiStreamViewController;

@interface TiStreamAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    TiStreamViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet TiStreamViewController *viewController;

@end


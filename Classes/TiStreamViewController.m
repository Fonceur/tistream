//
//  TiStreamViewController.m
//  TiStream
//
//  Created by Patrick Roy on 11-02-24.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TiStreamViewController.h"

@implementation TiStreamViewController

@synthesize tvAddress, tvPort, tvUsername;
@synthesize receivedData, tvPassword, tvMediaFileID;


-(IBAction) stream1{
	NSLog(@"Stream 1");
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self requestURL]]];
}

-(IBAction) stream2{
	NSLog(@"Stream 2");
	[self playMovieAtURL:[NSURL URLWithString:[self requestURL]]];
}


-(IBAction) stream3{
	NSLog(@"Stream 3");
	[self requestStream];
}


-(IBAction) stream4{
	NSLog(@"Stream 1");
}


-(IBAction) stream5{
	NSLog(@"Stream 1");
}

-(NSString *) requestURL{
    NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:tvUsername.text
                                   password:tvPassword.text persistence: NSURLCredentialPersistenceForSession];
    NSURLProtectionSpace *protectionSpace = [[NSURLProtectionSpace alloc]
                                             initWithHost:tvAddress.text
                                             port:[tvPort.text intValue]
                                             protocol:@"http"
                                             realm:@"SageTV Web Interface"
                                             authenticationMethod:NSURLAuthenticationMethodDefault];
    
    [[NSURLCredentialStorage sharedCredentialStorage] setDefaultCredential:credential forProtectionSpace:protectionSpace];
    [protectionSpace release];
    [credential release];
    
    /*NSString *url = [[NSString alloc] initWithFormat:
					 @"http://%@:%@@%@:%@/stream/HTTPLiveStreamingPlaylist?MediaFileId=%@",
					 tvUsername.text , tvPassword.text, tvAddress.text, 
					 tvPort.text, tvMediaFileID.text];*/
	NSString *url = [[NSString alloc] initWithFormat:@"http://%@:%@/stream/HTTPLiveStreamingPlaylist?MediaFileId=%@",
					 tvAddress.text, tvPort.text, tvMediaFileID.text];
	NSLog(@"URL = %@", url);
	
	return [url autorelease];
}

-(void)requestStream{
	NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:[self requestURL]]
											  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	//NSLog(@"Creating the connection...");
	// create the connection with the request and start loading the data
	NSURLConnection *theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
	if (theConnection) {
		// Create the NSMutableData to hold the received data.
		// receivedData is an instance variable declared elsewhere.
		receivedData = [[NSMutableData alloc] init];
	} else {
		// Inform the user that the connection failed.
		NSLog(@"Stream request failed!");
	}
}

-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    NSLog(@"We have a challenge!");
    if ([challenge previousFailureCount] == 0)
        [[challenge sender] useCredential:[NSURLCredential credentialWithUser:tvUsername.text
                password:tvPassword.text persistence:NSURLCredentialPersistenceForSession] forAuthenticationChallenge:challenge];
    else {
        NSLog(@"Authentication failed");
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
	
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
	
	//NSLog(@"Received something, resetting the buffer");
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    // Append the new data to receivedData.
 	//NSLog(@"Received something, adding to the buffer");
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    connection = nil;
    receivedData = nil;
	
    // inform the user
    NSLog(@"Connection failed! Error - %@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
	connection = nil;
    // do something with the data
    NSLog(@"Succeeded! Received %d bytes of data", [receivedData length]);
	NSString *data2 = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	receivedData = nil;

	NSString *fullFileName = [NSString stringWithFormat:@"%@/temp.m3u8", NSHomeDirectory()];
	NSLog(@"File: %@", fullFileName);
	NSFileManager *fileManager = [NSFileManager defaultManager];
	[fileManager removeItemAtPath:fullFileName error:NULL];
	[data2 writeToFile:fullFileName atomically:NO encoding:NSUTF8StringEncoding error:NULL];
	[data2 release];
	[self playMovieAtURL:[NSURL fileURLWithPath:fullFileName]];
}

- (void)playMovieAtURL:(NSURL*)url{
	// running iOS 3.2 or better
	mViewPlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:url];
	mViewPlayer.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	mViewPlayer.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
	[self presentModalViewController:mViewPlayer animated:YES];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieDidExitFullscreen:)
												 name:MPMoviePlayerPlaybackDidFinishNotification object:[mViewPlayer moviePlayer]];
} 

-(void) movieDidExitFullscreen:(NSNotification*) aNotification {
	MPMoviePlayerController *player = [aNotification object];
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification object:player];
	[player pause];
	[player stop];
	[mViewPlayer.view removeFromSuperview];
	[mViewPlayer release];
	
	mViewPlayer = nil;
}


- (void)dealloc {
    [super dealloc];
	[receivedData release];
	[tvAddress release];
	[tvPort release];
	[tvUsername release];
	[tvPassword release];
	[tvMediaFileID release];
}

@end

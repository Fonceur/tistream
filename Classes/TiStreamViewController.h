//
//  TiStreamViewController.h
//  TiStream
//
//  Created by Patrick Roy on 11-02-24.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h> 
#import <UIKit/UIKit.h>

@interface TiStreamViewController : UIViewController {
	NSMutableData *receivedData;
	UITextField *tvAddress;
	UITextField *tvPort;	
	UITextField *tvUsername;	
	UITextField *tvPassword;
	UITextField *tvMediaFileID;
	MPMoviePlayerViewController *mViewPlayer;
}

@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) IBOutlet UITextField *tvAddress;
@property (nonatomic, retain) IBOutlet UITextField *tvPort;
@property (nonatomic, retain) IBOutlet UITextField *tvUsername;
@property (nonatomic, retain) IBOutlet UITextField *tvPassword;
@property (nonatomic, retain) IBOutlet UITextField *tvMediaFileID;

-(IBAction) stream1;
-(IBAction) stream2;
-(IBAction) stream3;
-(IBAction) stream4;
-(IBAction) stream5;
-(NSString *) requestURL;
-(void) movieDidExitFullscreen:(NSNotification*) aNotification;
-(void) playMovieAtURL:(NSURL*) url;
-(void) requestStream;

@end

